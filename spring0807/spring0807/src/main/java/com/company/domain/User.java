package com.company.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "USER", schema = "CATALOG")
public class User {

    @Id
    private int id;
    private String username;
    private String password;
}
