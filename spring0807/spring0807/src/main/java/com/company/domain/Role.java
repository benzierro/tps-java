package com.company.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "ROLE", schema = "CATALOG")
public class Role {

        @Id
        private int id;
        private String name;

}
