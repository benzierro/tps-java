package com.company;

import java.util.Random;

public class Book {
    private String author;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void callException() throws TestExtention {
        Random rand = new Random();
        int a = rand.nextInt(10) + 6;

        if (a % 2 == 0) {
            throw new TestExtention("My super custom exception");
        }
    }

}
