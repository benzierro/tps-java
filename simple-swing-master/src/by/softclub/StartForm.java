package by.softclub;

import javax.swing.*;

public class StartForm extends JFrame {
    private JPanel panel1;
    private JTextField textField1;
    private JButton button1;

    public StartForm() {
        setContentPane(panel1);
        setVisible(true);

        setSize(500, 500);

        button1.addActionListener(
                listener -> {
                    JOptionPane.showMessageDialog(button1, "Не надо было нажимать на эту кнопку", "Информация", JOptionPane.WARNING_MESSAGE);
                }
        );

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }
}
