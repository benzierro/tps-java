package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        List<Car> list = new ArrayList<Car>();

        for (int i = 0; i < 5; i++) {
            System.out.print("Please enter model of " + (i + 1) + " car: ");
            Scanner myObj = new Scanner(System.in);  // Create a Scanner object
            String model = myObj.nextLine();  // Read user input
            list.add(new Car(model));
        }
        System.out.println(list.toString());

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        for (Car car : list) {
            System.out.println(car);
        }

        list.forEach(car -> System.out.println(car));

        List<String> models = list.stream()
                .filter(item -> item.getModel().length() > 4)
                .distinct()
                .map(item -> item.getModel() + "test")
                .collect(Collectors.toList());

        System.out.println("end");
    }
}
