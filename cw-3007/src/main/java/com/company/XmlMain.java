package com.company;

import com.company.domain.Person;
import com.company.service.JsonMapperService;
import com.company.service.MapperService;
import com.company.service.XmlMapperService;

import java.util.ArrayList;
import java.util.List;

public class XmlMain {
    public static void main(String[] args) {

        MapperService mapperService = new XmlMapperService();
        List<Person> persons = new ArrayList<>();

        Person person = new Person();
        person.setId(1);
        person.setName("Jason");
        person.setBirthday("01.01.2001");
        persons.add(person);

        Person person2 = new Person();
        person2.setId(2);
        person2.setName("Peter");
        person2.setBirthday("02.02.2002");
        persons.add(person2);

        String xml = mapperService.map(person);
        System.out.println(xml);
        Person pp1 = mapperService.unmap(xml);
        System.out.println(pp1.toString());

        xml = mapperService.map(person2);
        System.out.println(xml);
        Person pp2 = (Person) mapperService.unmapObject(xml, Person.class);
        System.out.println(pp2.toString());
    }
}
