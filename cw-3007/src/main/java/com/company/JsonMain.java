package com.company;

import com.company.domain.Person;
import com.company.service.JsonMapperService;
import com.company.service.MapperService;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class JsonMain {

    public static void main(String[] args) {
        MapperService mapperService = new JsonMapperService();
        List<Person> persons = new ArrayList<>();

        Person person = new Person();
        person.setId(1);
        person.setName("Jason");
        person.setBirthday("01.01.2001");
        persons.add(person);

        Person person2 = new Person();
        person2.setId(2);
        person2.setName("Peter");
        person2.setBirthday("02.02.2002");
        persons.add(person2);

        List<Person> pp = (List<Person>) mapperService.readFromFile(ArrayList.class, args[0]);
        System.out.println("1111111111111   " + pp);
        mapperService.saveToFile(persons, args[0]);
        String json = mapperService.map(persons);
        System.out.println(json);

        String json2 = mapperService.map(person);
        Person person3 = mapperService.unmap(json2);
        System.out.println(person3);
        json2 = mapperService.map(person2);
        person3 = mapperService.unmap(json2);
        System.out.println(person3);

        List<Person> persons2 = mapperService.unmapArray(json);
        System.out.println(persons2);

        person3 = (Person) mapperService.unmapObject(json2, Person.class);
        System.out.println(person3);

	}
}
