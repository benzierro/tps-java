package com.company.shapes;

public class Square extends Shape {
    public Square(int side) {
        this.side = side;
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
    }

    private int side;

    @Override
    public void showArea() {
        System.out.println("Area of square = " + area);
    }
    @Override
    public void calcArea() { area = Math.pow(side, 2); }
}
