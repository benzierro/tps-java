package com.sc;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class MySecondTest {

    @Test
    public void testEven() {
        OddCheck oddCheck = new OddCheck();
        List<Integer> evenVariables = new ArrayList<>(
                Arrays.asList(2, 4, 6, 8, 10)
        );
        for (Integer val : evenVariables) {
            boolean isOdd = oddCheck.checkNumber(val);
            Assert.assertTrue(isOdd);
        }

    }

    @Test
    public void testOdd() {
        OddCheck oddCheck = new OddCheck();
        List<Integer> oddVariables = new ArrayList<>(
                Arrays.asList(1, 3, 5, 7, 9)
        );
        for (Integer val : oddVariables) {
            boolean isOdd = oddCheck.checkNumber(val);
            Assert.assertFalse(isOdd);
        }
    }
}
