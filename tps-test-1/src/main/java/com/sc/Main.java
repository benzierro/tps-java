package com.sc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        WordsCount wordsCount = new WordsCount();
        List<String> lines = new ArrayList<>();
        lines.add("qwe xzc asd xzc qwe xzc");
        HashMap<String, Integer> words;
        words = wordsCount.countWords(lines);
        System.out.println(words.toString());
    }
}
