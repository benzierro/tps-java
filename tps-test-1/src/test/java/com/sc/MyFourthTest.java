package com.sc;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class MyFourthTest {

    @Test
    public void testCountPositive() {
        WordsCount wordsCount = new WordsCount();
        List<String> lines = new ArrayList<>();
        lines.add("qwe asd qwe xzc.");
        lines.add("xzc, xzc?");
        lines.add("qqqqq!");
        HashMap<String, Integer> words;
        words = wordsCount.countWords(lines);
        HashMap<String, Integer> expectedWords = new HashMap<>();
        expectedWords.put("qwe", 2);
        expectedWords.put("xzc", 3);
        expectedWords.put("qqqqq", 1);
        expectedWords.put("asd", 1);
        assertTrue(expectedWords.equals(words));
    }

    @Test
    public void testCountNegetive() {
        WordsCount wordsCount = new WordsCount();
        List<String> lines = new ArrayList<>();
        lines.add("asfhnwq kksdksd");
        lines.add("asdqwe lllllllk");
        lines.add("zxcasdw");
        HashMap<String, Integer> words;
        words = wordsCount.countWords(lines);
        HashMap<String, Integer> expectedWords = new HashMap<>();
        expectedWords.put("asfhnwq", 2);
        expectedWords.put("lllllllk", 3);
        expectedWords.put("kksdksd", 1);
        expectedWords.put("asdqwe", 1);
        expectedWords.put("zxcasdw", 1);
        assertFalse(expectedWords.equals(words));
    }
}
