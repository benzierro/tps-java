package com.company.shapes;

public class Circle extends Shape {

    @Override
    public void showArea() {
        System.out.println("( Pi * R^2 ) / 2");
    }

    public void test() {
        System.out.println("test");
    };
}
