package com.company.repository;

import com.company.domain.Animal;
import com.company.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository  extends JpaRepository<User, String> {
    User findByUsername(String username);
}