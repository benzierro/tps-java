package com.company;

import java.util.*;

public class Main {

    static Map<Integer, List<Article>> articleMap = new HashMap<>();

    public static void main(String[] args) {

        /*Map<String, Box> boxMap = new HashMap<>();

        Box box1 = new Box();
        box1.setApples(5);
        Box box2 = new Box();
        box2.setApples(8);
        Box box3 = new Box();
        box3.setApples(8);

        List<Box> boxList = new ArrayList<>();
        boxList.add(box1);
        boxList.add(box2);
        boxList.add(box3);

        boxMap.put("Kalenchik", box1);
        boxMap.put("Tokarev", box3);
        //boxMap.put("Lyahor", box2);


        if (!boxMap.containsValue(box2)){
            boxMap.put("Lyahor", box2);
        }
        boxMap.putIfAbsent("Lyahor", box2);
        if (boxMap.get("Lyahor") == null){
            boxMap.put("Lyahor", box2);
        }


        boxMap.forEach((key, box) -> System.out.println(box.toString()));
        System.out.println(boxMap.toString());
        System.out.println(boxMap.get("Lyahor").getApples());*/


        for (int i = 0; i < 10; i++) {
            Random rand = new Random();
            int page = rand.nextInt(3) + 1;
            System.out.println(page);
            List<Article> articles = getArticles(page);
            System.out.println(articles.toString());
        }
    }

    //Page from 1 to 3
    public static List<Article> getArticles(Integer page) {
        List<Article> articles = null;

        if (!articleMap.containsKey(page)) {
            if (page == 1) {
                articles = new ArrayList<>();
                articles.add(new Article("1"));
                articles.add(new Article("2"));
                articles.add(new Article("3"));
                articleMap.put(page, articles);
                System.out.println("1 page hash");
            }
            if (page == 2) {
                articles = new ArrayList<>();
                articles.add(new Article("4"));
                articles.add(new Article("5"));
                articles.add(new Article("6"));
                articleMap.put(page, articles);
                System.out.println("2 page hash");
            }
            if (page == 3) {
                articles = new ArrayList<>();
                articles.add(new Article("7"));
                articles.add(new Article("8"));
                articleMap.put(page, articles);
                System.out.println("3 page hash");
            }
        }
        else {
            articles = articleMap.get(page);
        }

        return articles;
    }

}
