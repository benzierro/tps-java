package com.company.service;

import com.company.domain.Person;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class JsonMapperService implements MapperService {

    @Override
    public String map(Object object) {
        return new Gson().toJson(object);
    }

    @Override
    public Person unmap(String str) {
        return new Gson().fromJson(str, Person.class);
    }

    @Override
    public Object unmapObject(String str, Class classOfT) {
        return new Gson().fromJson(str, classOfT);
    }

    @Override
    public ArrayList<Person> unmapArray(String str) {
        return new Gson().fromJson(str, ArrayList.class);
    }

    @Override
    public void saveToFile(Object object, String path) {
        try
        {
            //Files.write(Paths.get(path),new GsonBuilder().setPrettyPrinting().create().toJson(object).getBytes());
            Files.write(Paths.get(path),new Gson().toJson(object).getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public Object readFromFile(Class classOfT, String path) {
        String str = "";
        try {
            str = new String(Files.readAllBytes(Paths.get(path)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new Gson().fromJson(str, classOfT);
        //return new GsonBuilder().setPrettyPrinting().create().fromJson(str, classOfT);
    }

}
