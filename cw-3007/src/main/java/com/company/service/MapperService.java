package com.company.service;

import com.company.domain.Person;

import java.util.ArrayList;

public interface MapperService {
    String map(Object object);
    Person unmap(String str);
    Object unmapObject(String str, Class classOfT);
    ArrayList<Person> unmapArray(String str);

    void saveToFile(Object object, String path);
    Object readFromFile(Class classOfT, String path);
}
