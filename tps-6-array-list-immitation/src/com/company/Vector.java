package com.company;

import java.util.Arrays;

public class Vector {
    Car[] cars = new Car[5];
    int currentCar = 0;

    public void addCar(Car car) {
        if (currentCar < cars.length){
            cars[currentCar] = car;
        } else {
            Car[] tmpCars = new Car[cars.length * 2];
            for (int i = 0; i < cars.length; i++) {
                tmpCars[i] = cars[i];
            }
            cars = new Car[cars.length * 2];
            for (int i = 0; i < currentCar; i++) {
                cars[i] = tmpCars[i];
            }
            cars[currentCar] = car;
        }
        currentCar++;
    }


    @Override
    public String toString() {
        String str = "Vector{cars=";
        for (int i = 0; i < cars.length; i++) {
            if (cars[i] != null) {
                str += " " + cars[i].toString();
            }
        }
        str += "}";
        return str;
    }

    public void showMaxLength() {
        System.out.println(cars.length);
    }
}
