package com.company;

import java.sql.*;

public class Main {

    public static void main(String[] args) {
        try (Connection connection = DriverManager.getConnection("jdbc:postgresql://86.57.237.193:5432/census", "postgresadmin", "admin123")) {

            System.out.println("Java JDBC PostgreSQL Example");


            System.out.println("Connected to PostgreSQL database!");
            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery("SELECT * FROM catalog.ate q WHERE q.parent_code = " + args[0]);
            while (resultSet.next()) {
                System.out.println(resultSet.getString("fullname"));
            }

        } /*catch (ClassNotFoundException e) {
            System.out.println("PostgreSQL JDBC driver not found.");
            e.printStackTrace();
        }*/ catch (SQLException e) {
            System.out.println("Connection failure.");
            e.printStackTrace();
        }
    }

}
