package com.company;

public class Main {

    public static void main(String[] args) {
        Vector vector = new Vector();
        vector.addCar(new Car("1",1));
        vector.addCar(new Car("2",2));
        vector.addCar(new Car("3",3));
        System.out.println(vector.toString());
        vector.showMaxLength();
        vector.addCar(new Car("4",4));
        vector.addCar(new Car("5",5));
        vector.addCar(new Car("6",6));
        vector.addCar(new Car("7",7));
        System.out.println(vector.toString());
        vector.showMaxLength();
    }
}
