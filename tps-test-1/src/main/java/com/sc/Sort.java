package com.sc;

import java.util.Arrays;

public class Sort {

    public int[] sort(int[] array) {
        Arrays.sort(array);
        return array;
    }
}
