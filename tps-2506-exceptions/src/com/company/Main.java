package com.company;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<String> linesFromFile = null;
        for (int i = 1; i < 11; i++) {
            try {
                Charset ch = Charset.forName("CP866");
                linesFromFile = Files.readAllLines(Paths.get("./resources/file" + i + ".txt"), ch);
                System.out.println("File file" + i + ".txt is found.");
                i = 11;
            } catch (IOException e) {
                System.out.println("File file" + i + ".txt not found.");
            }
        }
        System.out.println(linesFromFile);

        //Book book = new Book();
        //book.callException();
        //test2();
    }

    public static void test2() throws IOException {
        test();
    }

    public static void test() throws IOException {
        List<String> lines = Files.readAllLines(Paths.get("/home/student/test.txt"), StandardCharsets.UTF_8);
        System.out.println(lines.size());

//        try {
//            lines = Files.readAllLines(Paths.get("/home/student/test.txt"), StandardCharsets.UTF_8);
//
//            //int a = 5;
//            //int b = a / 0;
//
//            //Book book = null;
//            //String str = book.getAuthor();
//
//            System.out.println(lines.size());
//        } catch (ArithmeticException | IOException e1) {
//            e1.printStackTrace();
//        } finally {
//            System.out.println("in finally section");
//        }
//
//        System.out.println("after try-catch-finally");
    }
}


