package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
//        List<String> strList = new ArrayList<String>();
//        Random rand = new Random();
//        int strCount = rand.nextInt(6) + 2;
//
//        System.out.println("Please enter " + strCount + " distinct strings.");
//        for (int i = 0; i < strCount; i++) {
//            System.out.print("Please enter the " + (i + 1) + " string: ");
//            Scanner myObj = new Scanner(System.in);  // Create a Scanner object
//            String str = myObj.nextLine();  // Read user input
//            if (strList.contains(str)){
//                System.out.println("ERROR! List containt this string. ");
//            }
//            else {
//                strList.add(str);
//            }
//        }
//        System.out.println("List containts " + strList.size() + " elements.");
//        System.out.println(strList.toString());


//        List<Person> personList = new ArrayList<>();
//        personList.add(new Person(25, "Eugene"));
//        Person person = new Person(25, "Eugene");
//        if (personList.contains(person)) {
//            System.out.println("ERROR! List containt this person (" + person.toString() + ").");
//        } else {
//            personList.add(person);
//        }
//        person.setName("Victor");
//        person.setAge(26);
//        if (personList.contains(person)) {
//            System.out.println("ERROR! List containt this person (" + person.toString() + ").");
//        } else {
//            personList.add(person);
//        }
//        System.out.println("List containts " + personList.size() + " persons.");
//        System.out.println(personList.toString());


        Set<Person> people = new HashSet<>();
        people.add(new Person(25, "Eugene"));
        people.add(new Person(25, "Eugene"));
        System.out.println(people.toString());

    }
}
