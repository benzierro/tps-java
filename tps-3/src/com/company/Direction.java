package com.company;

public enum Direction {
    LEFT(1, "Влево"),
    RIGHT(2, "Вправо"),
    UP(3, "Вверх"),
    DOWN(4, "Вниз");

    int value;
    String name;

    Direction(int value, String name) {
        this.value = value;
        this.name = name;
    }

}
