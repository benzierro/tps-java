package com.company;

public class Car {

    public Car(String name, int countOfDors) {
        this.name = name;
        this.countOfDors = countOfDors;
    }
    private String name;
    private int countOfDors;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountOfDors(int countOfDors) {
        this.countOfDors = countOfDors;
    }

    public int getCountOfDors() {
        return countOfDors;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", countOfDors=" + countOfDors +
                '}';
    }
}
