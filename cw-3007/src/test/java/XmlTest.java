import com.company.domain.Person;
import com.company.service.XmlMapperService;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class XmlTest {

    @Test
    public void testConvertToXml() {
        XmlMapperService xmlMapperService = new XmlMapperService();
        String result = xmlMapperService.map(new Person(1,"JUnit", "06.07.2019"));
        assertEquals("<Person><id>1</id><name>JUnit</name><birthday>06.07.2019</birthday></Person>", result);
    }

    @Test
    public void testWrongConvertToXmlNegative() {
        XmlMapperService xmlMapperService = new XmlMapperService();
        String result = xmlMapperService.map(new Person(2,"JUnit", "06.07.2010"));
        assertNotEquals("<Person><id>1</id><name>JUnit</name><birthday>06.07.2019</birthday></Person>", result);
    }

    @Test
    public void testConvertFromXml() {
        XmlMapperService xmlMapperService = new XmlMapperService();
        Person result = xmlMapperService.unmap("<Person><id>2</id><name>Xml</name><birthday>01.01.2001</birthday></Person>");
        assertEquals(new Person(2,"Xml", "01.01.2001"), result);
    }

    @Test
    public void testConvertFromXmlNegative() {
        XmlMapperService xmlMapperService = new XmlMapperService();
        Person result = xmlMapperService.unmap("<Person><id>2</id><name>Xml</name><birthday>01.01.2001</birthday></Person>");
        assertNotEquals(new Person(3,"Xml", "01.01.2010"), result);
    }

}
