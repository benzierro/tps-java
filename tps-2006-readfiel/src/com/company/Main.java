package com.company;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        List<String> lines = null;
        try {
            lines = Files.readAllLines(Paths.get("/home/student/test.txt"), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        List<String> words = new ArrayList<>();
        lines.forEach(tmpStr ->
                words.addAll(Arrays.asList(tmpStr.replaceAll("[().,;?!«»]", "").replaceAll("[\\s]{2,}", " ").toLowerCase().split(" "))));
        Map<String, Integer> wordsMap = new HashMap<>();
        words.stream().distinct().forEach(tmpStr -> wordsMap.put(tmpStr, Collections.frequency(words, tmpStr)));

        System.out.println(wordsMap);

        List<String> wordsForArchive = new ArrayList<>();
        wordsMap.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .forEach(e -> wordsForArchive.add(e.getKey()));

        int i = 1;
        Map<String, Integer> wordsMapArchive = new HashMap<>();
        for (String tmpStr : wordsForArchive) {
            wordsMapArchive.put(tmpStr, i++);
        }

        List<String> linesArchived = null;
        for (String tmpStr : lines) {
            String newStr = tmpStr.replaceAll("[().,;?!«»]", "").replaceAll("[\\s]{2,}", " ").toLowerCase();
            for (String tmpStr2 : wordsForArchive) {
                newStr = newStr.replaceAll(tmpStr2, wordsMapArchive.);
            }

        }
        System.out.println(linesArchived);
    }
}
