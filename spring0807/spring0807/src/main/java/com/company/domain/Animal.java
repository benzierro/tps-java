package com.company.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "ANIMAL", schema = "CATALOG")
public class Animal {

    @Id
    private String code;
    private String nameRu;
    private String nameBy;
    private String parentCode;
}
