package com.company;

import com.company.shapes.Circle;
import com.company.shapes.Shape;
import com.company.shapes.Square;
import com.company.shapes.Triangle;

import java.math.BigDecimal;

public class Main {

    static int c;

    public static void main(String[] args) {
        int a = 1;
        int b = 2;

        int c = a + b;

        System.out.println(c);

        System.out.println("Array length = " + args.length);

        int sum = 0;

        for (int i = 0; i < args.length; i++) {
            System.out.println(args[i]);
            sum = sum + Integer.parseInt(args[i]);
        }
        System.out.println("Sum of array elements = " + sum);

    }
}
