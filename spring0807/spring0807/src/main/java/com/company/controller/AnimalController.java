package com.company.controller;

import com.company.domain.Animal;
import com.company.service.AnimalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class AnimalController {

    @Autowired
    AnimalService animalService;

    @RequestMapping(value = "/animal", method = RequestMethod.GET)
    public ModelAndView showForm() {
        return new ModelAndView("animalHome", "animal", new Animal());
    }

    @RequestMapping(value = "/addAnimal", method = RequestMethod.POST)
    public String submit(@Valid @ModelAttribute("animal")Animal animal,
                         BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }
        model.addAttribute("code", animal.getCode());
        model.addAttribute("nameBy", animal.getNameBy());
        model.addAttribute("nameRu", animal.getNameRu());
        model.addAttribute("parentCode", animal.getNameRu());
        animalService.addNewAnimal(animal);
        return "animalView";
    }
}

