package com.company;

public class Main {

    private static final int COUNT_OF_CYCLE = 5;

    /*private static final int LEFT = 0;
    private static final int RIGHT = 1;
    private static final int UP = 2;
    private static final int DOWN = 3;*/

    public static void main(String[] args) {

        Animal[] array1 = new Dog[2];
        iTest[] array2 = new Dog[2];
        iHomeAnimal[] array3 = new Dog[2];

        iHomeAnimal test= new Dog();
        Dog dog= new Dog();
        array1[0] = (Dog)test;
        array2[0] = (Dog)test;
        array3[0] = test;
        array1[1] = dog;
        array2[1] = dog;
        array3[1] = dog;

        array1[0].move();
        ((Dog)array2[0]).move();
        ((Dog)array3[0]).move();
        dog.move();

        ((Dog)array1[0]).bringBall();
        ((Dog)array2[0]).bringBall();
        array3[0].bringBall();
        dog.bringBall();

        dog.bringBall(dog.test());
        dog.bringBall(array2[0].test());

        /*Car car = new Car(Direction.UP);

        car.move();

        int a = Math.abs(5);
        for (int i = 0; i < COUNT_OF_CYCLE; i++) {
        }

        System.out.println(Color.TEAL.toString());
        System.out.println(Color.ORANGE.toString());
        System.out.println(Color.BLACK.toString());
        System.out.println(Color.WHITE.toString());

         */
    }
}
