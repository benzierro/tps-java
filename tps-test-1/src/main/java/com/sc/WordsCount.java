package com.sc;

import java.util.*;

public class WordsCount {

    public HashMap<String, Integer> countWords(List<String> lines) {
        HashMap<String, Integer> result = new HashMap<>();
        List<String> words = new ArrayList<>();
        lines.forEach(tmpStr ->
                words.addAll(Arrays.asList(tmpStr.replaceAll("[().,;?!«»]", "").replaceAll("[\\s]{2,}", " ").toLowerCase().split(" "))));
        words.stream().distinct().forEach(tmpStr -> result.put(tmpStr, Collections.frequency(words, tmpStr)));
        return result;
    }

}
