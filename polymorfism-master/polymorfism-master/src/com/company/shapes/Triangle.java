package com.company.shapes;

public class Triangle extends Shape {
    public Triangle(double base, double height) {
        this.base = base;
        this.height = height;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    private double base;

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    private  double height;

    @Override
    public void showArea() {
        System.out.println("Area of triangle = " + area);
    }
    @Override
    public void calcArea() { area = base * height / 2; }
}
