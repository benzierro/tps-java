package com.company.controller;


import com.company.domain.Animal;
import com.company.repository.AnimalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class WelcomeController {

    // inject via application.properties
    @Value("${welcome.message}")
    private String message;

    @Autowired
    private AnimalRepository animalRepository;

    @PostConstruct
    public void init() {
        tasks = animalRepository.findAll();
    }

    //private List<String> tasks = Arrays.asList("a", "b", "c", "d", "e", "f", "g");
    private List<Animal> tasks = new ArrayList<>();

    @GetMapping("/")
    public String main(Model model) {
//        List<Animal> tasks = new ArrayList<>();
//        Animal animal1 = new Animal();
//        animal1.setCode("21");
//        animal1.setNameRu("Cat");
//        tasks.add(animal1);
//        Animal animal2 = new Animal();
//        animal2.setCode("44");
//        animal2.setNameRu("Dog");
//        tasks.add(animal2);
//        Animal animal3 = new Animal();
//        animal3.setCode("28");
//        animal3.setNameRu("Pig");
//        tasks.add(animal3);
        model.addAttribute("message", message);
        model.addAttribute("tasks", tasks);

        return "welcome"; //view
    }

}
