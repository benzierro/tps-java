package com.company.shapes;

public abstract class Shape {
    protected double area;

    public void showArea() {
        System.out.println("show abstract area");
    }
    public abstract void calcArea();
}
