package com.sc;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MyThirdTest {

    @Test
    public void testSort() {
        Sort sort = new Sort();
        int[] array = {3, 5, 1};
        int[] resultArray = sort.sort(array);
        int[] expectedArray = {1, 3, 5};
        for (int i = 0; i < array.length; i++) {
            assertEquals(resultArray[i], expectedArray[i]);
        }
    }

    @Test
    public void testSort2() {
        Sort sort = new Sort();
        int[] array = {7, 5, 4};
        int[] resultArray = sort.sort(array);
        int[] expectedArray = {4, 7, 5};
        boolean isPassed = false;
        for (int i = 0; i < array.length; i++) {
            if (resultArray[i] != expectedArray[i]) {
                isPassed = true;
            };
        }
        assertTrue(isPassed);
    }

}
