package com.company;

public enum Color {
    TEAL(0,128,128),
    ORANGE(255,165,0),
    BLACK(0, 0, 0),
    WHITE(255, 255, 255);

    int red;
    int green;
    int blue;

    Color(int red, int green, int blue){
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    @Override
    public String toString() {
        return this.name() + " rgb(" + "red=" + red + ", green=" + green + ", blue=" + blue + ')';
    }
}
