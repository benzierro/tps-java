package com.company;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Main {

    public static void main(String[] args) {
//        Set<String> set = new TreeSet<>();
//        set.add("5");
//        set.add("1");
//        set.add("3");
//        System.out.println(set);
//
//        Set<Box> boxTree = new TreeSet<>();
//        boxTree.add(new Box("1"));
//        boxTree.add(new Box("3"));
//        boxTree.add(new Box("5"));
//        System.out.println(boxTree.toString());


//        Queue<String> queue = new PriorityQueue<>();
//        queue.add("Hello");
//        queue.add("World");
//        System.out.println(queue.peek());
//        System.out.println(queue.poll());
//        System.out.println(queue.poll());
//        System.out.println(queue.poll());
//        System.out.println();
//
//        Stack<String> stack = new Stack<>();
//        stack.push("Hello");
//        stack.push("World");
//        System.out.println(stack.peek());
//        System.out.println(stack.pop());
//        System.out.println(stack.pop());
//        System.out.println(stack.pop());

        Path path = Paths.get("/home/student/tps-java/tps-190618-tree-set/files/test.txt");
        List<String> list = null;

        Set<Article> articleTree = new TreeSet<>();
        try {
            list = Files.readAllLines(path);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < list.size(); i++) {
            String[] tmpStr = list.get(i).split(" ");
            if (tmpStr.length == 3) {
                articleTree.add(new Article(tmpStr[0], tmpStr[1], tmpStr[2]));
            }
            else {
                System.out.println("In file " + (i + 1) + " line containts not 3 fields.");
            }
        }
        System.out.println(articleTree);

    }
}
