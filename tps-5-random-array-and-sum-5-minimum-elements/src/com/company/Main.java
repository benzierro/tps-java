package com.company;

import java.util.Random;

public class Main {

    public static void printIntArray(int[] arrayForPrint) {
        System.out.print("Array contains values: ");
        for (int i = 0; i < arrayForPrint.length; i++) {
            System.out.print(arrayForPrint[i] + " ");
        }
        System.out.println();
    }
    public static void sortArrayAsc(int[] array) {
        int minValue;
        int minElement;
        for (int i = 0; i < array.length; i++) {
            minValue = array[i];
            minElement = i;
            for (int j = i + 1; j < array.length; j++) {
                if (minValue > array[j]) {
                    minValue = array[j];
                    minElement = j;
                }
            }
            array[minElement] = array[i];
            array[i] = minValue;
        }
    }

    public static void main(String[] args) {
	    int[] array = new int[20];

        Random rand = new Random();
        for (int i = 0; i < 20; i++) {
            array[i] = rand.nextInt(100);
        }

        printIntArray(array);
        // asc sort
        sortArrayAsc(array);
        printIntArray(array);
        int sum = 0;
        for (int i = 0; i < 5; i++) {
            sum += array[i];
        }
        System.out.println("Sum of 5 minimal values elements of array = " + sum);
    }
}
