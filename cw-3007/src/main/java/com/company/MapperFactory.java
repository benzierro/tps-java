package com.company;

import com.company.service.JsonMapperService;
import com.company.service.MapperService;
import com.company.service.XmlMapperService;

public final class MapperFactory {

    private MapperFactory(){};

    public static MapperService createMapperService(String str) throws Exception {
        if (str.toLowerCase().equals("xml")) {
            return new XmlMapperService();
        }
        if (str.toLowerCase().equals("json")) {
            return new JsonMapperService();
        }
        throw new Exception("Type of mapper not found.");
    }

}
