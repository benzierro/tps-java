package com.company;

import com.company.domain.Person;
import com.company.service.MapperService;

import javax.swing.*;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class MainForm extends JFrame {
    private JFormattedTextField birthdayTF;
    private JPanel MainPanel;
    private JTextField pathTF;
    private JTextField idTF;
    private JTextField nameTF;
    private JButton saveBtn;
    private JRadioButton jsonRB;
    private JRadioButton xmlRB;
    private JTextArea listTA;
    private JButton addInListBtn;
    private JButton clearListButton;
    private JButton writeListToFileButton;
    private List<Person> personList = new ArrayList<>();

    public MainForm() {
        setContentPane(MainPanel);
        setVisible(true);

        setSize(900, 300);
//        JScrollPane scroll = new JScrollPane (listTA,
//                JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
//        JFrame frame = new JFrame ("Test");
//        frame.add(scroll);
//        frame.setVisible (true);

//        DateFormat format = new SimpleDateFormat("DD.MM.YYYY");
//        birthdayTF.setFormatterFactory(new DefaultFormatterFactory(new DateFormatter((DateFormat)format)));
        listTA.setPreferredSize(new Dimension(900, 200));
        listTA.setLineWrap(true);
        listTA.append("id name birthday\n");
        listTA.setEditable(false);

        MaskFormatter maskFormatter = null;
        try {
            maskFormatter = new MaskFormatter("##.##.####");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        maskFormatter.setPlaceholderCharacter('_');
        birthdayTF.setFormatterFactory(new DefaultFormatterFactory(maskFormatter));

        ButtonGroup bG = new ButtonGroup();
        bG.add(jsonRB);
        bG.add(xmlRB);


//        try {
//            Date date1 = new SimpleDateFormat("dd-MM-yyyy").parse("fgg-05-1993");
//            System.out.println(date1);
//        } catch (ParseException e) {
//            e.printStackTrace();
//            JOptionPane.showMessageDialog(new JButton(), "Incorrect data format.", "Информация", JOptionPane.WARNING_MESSAGE);
//        }


        addInListBtn.addActionListener(
                listener -> {
                    Person person = new Person();

                    try {
                        person.setId(Integer.parseInt(idTF.getText()));
                        String regex = "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";
                        if (!birthdayTF.getText().matches(regex)) {
                            throw new ParseException("!", 0);
                        }
                        person.setBirthday(birthdayTF.getText());
                        person.setName(nameTF.getText());
                    } catch (NumberFormatException outputText) {
                        JOptionPane.showMessageDialog(new JButton(), "Для поля Id необходимо ввести числовое значение.", "Информация", JOptionPane.ERROR_MESSAGE);
                        return;
                    } catch (ParseException e) {
                        JOptionPane.showMessageDialog(new JButton(), "Для поля Birthday необходимо ввести корректную дату.", "Информация", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    personList.add(person);
                    listTA.append(person.getId() + " " + person.getName() + " " + person.getBirthday() + "\n");
                }
        );

        clearListButton.addActionListener(
                listener -> {
                    listTA.selectAll();
                    listTA.replaceSelection("");
                    personList = new ArrayList<>();
                    listTA.append("id name birthday\n");
                }
        );

        writeListToFileButton.addActionListener(
                listener -> {
                    String str = jsonRB.isSelected() ? "json" : "xml";
                    MapperService mapperService = null;
                    try {
                        mapperService = MapperFactory.createMapperService(str);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (!pathTF.getText().equals("")) {
                        mapperService.saveToFile(personList, pathTF.getText());
                    } else {
                        mapperService.saveToFile(personList, "./src/main/resources/output2." + str);
                    }
                }
                );

        saveBtn.addActionListener(
                listener -> {
                    String str = jsonRB.isSelected() ? "json" : "xml";
                    MapperService mapperService = null;
                    try {
                        mapperService = MapperFactory.createMapperService(str);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Person person = new Person();
                    try {
                        person.setId(Integer.parseInt(idTF.getText()));
                        String regex = "^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[1,3-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$";
                        if (!birthdayTF.getText().matches(regex)) {
                            throw new ParseException("!", 0);
                        }
                        person.setBirthday(birthdayTF.getText());
                        person.setName(nameTF.getText());
//                        int dd = Integer.parseInt(birthdayTF.getText().substring(0,2));
//                        int mm = Integer.parseInt(birthdayTF.getText().substring(3,5));
//                        if (dd > 31) {
//                            throw new ParseException("dd", 0);
//                        }
//                        if (mm > 12) {
//                            throw new ParseException("mm", 0);
//                        }
//                        person.setBirthday(birthdayTF.getText());
                    } catch (NumberFormatException outputText) {
                        JOptionPane.showMessageDialog(new JButton(), "Для поля Id необходимо ввести числовое значение.", "Информация", JOptionPane.ERROR_MESSAGE);
                        return;
                    } catch (ParseException e) {
                        JOptionPane.showMessageDialog(new JButton(), "Для поля Birthday необходимо ввести корректную дату.", "Информация", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    if (!pathTF.getText().equals("")) {
                        mapperService.saveToFile(person, pathTF.getText());
                    } else {
                        mapperService.saveToFile(person, "./src/main/resources/output5." + str);
                    }
                }
        );

    }
}
