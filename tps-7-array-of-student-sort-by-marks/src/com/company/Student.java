package com.company;

public class Student implements Comparable<Student> {

    private int id;
    private String name;
    private String lastName;
    private int mark;

    public Student(int id, String name, String lastName, int mark) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.mark = mark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", mark=" + mark +
                '}';
    }

    @Override
    public int compareTo(Student student) {
        if(this.mark < student.getMark()) return -1;
        if(this.mark == student.getMark()) return 0;
        else return 1;
    }
}
