package com.company.service;

import com.company.domain.Animal;
import com.company.repository.AnimalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AnimalService {

    @Autowired
    private AnimalRepository repository;

    @Transactional
    public void addNewAnimal(Animal animal) {
        repository.save(animal);
    }
}
