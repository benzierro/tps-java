package com.company;

import javax.xml.crypto.Data;

public class Main {

    public static void main(String[] args) {
//        DataReader dr = getDataReader(args[0]);
//        dr.test();
//        System.out.println(dr.getData());

        System.out.println(test(new DataReader() {
            @Override
            public String getData() {
                return "HELLO ANONYMOUS CLASS";
            }
        }));

        System.out.println(test(() -> "HELLO ANONYMOUS CLASS"));

        System.out.println(test(() -> {
            int i = 2;
            return "HELLO ANONYMOUS CLASS" + i;
        }));


    }

    public static String test(DataReader dataReader) {
        return dataReader.getData();
    }

    public static DataReader getDataReader(String name) {
        if (name.equals("FILE")) {
            return new FileReader();
        } else if (name.equals("DB")) {
            return new DbReader();
        } else {
            return new HttpReader();
        }
    }
}