package com.company;

public class Dog extends Animal implements iTest, iHomeAnimal {
    @Override
    public void bringBall() {
        System.out.println("Bring ball");
    }

    @Override
    public void bringBall(int t) {
        System.out.println("Bring ball " + t);

    }
}
