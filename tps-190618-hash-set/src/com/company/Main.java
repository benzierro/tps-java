package com.company;

import java.io.*;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.Set;

public class Main {

    public static void main(String[] args) {

//        Set<String> strSet = new HashSet<>();
//        strSet.add("First string");
//        strSet.add("Second string");
//        strSet.add("Third string");
//        strSet.add("First string");
//        System.out.println(strSet.size());
//
//
//        Set<Box> boxSet = new HashSet<>();
//        boxSet.add(new Box("1"));
//        boxSet.add(new Box("2"));
//        boxSet.add(new Box("3"));
//        boxSet.add(new Box("3"));
//        System.out.println(strSet.size());
//
//
//        Set<Article> articleSet = new HashSet<>();
//        articleSet.add(new Article("1", "test", "asdfg"));
//        articleSet.add(new Article("2", "test", "asdfg"));
//        articleSet.add(new Article("1", "test2", "qwert"));
//        articleSet.add(new Article("1", "test3", "xzcvc"));
//        articleSet.add(new Article("1", "test3", "asfsddg"));
//        System.out.println(articleSet.size());
//
//        Article article = new Article("1", "test", "asdfg");
//        try {
//            FileOutputStream fos = new FileOutputStream("/home/student/articleSet.out");
//            ObjectOutputStream oos = new ObjectOutputStream(fos);
//            oos.writeObject(articleSet);
//            oos.flush();
//            oos.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

        //Article article = null;
        Set<Article> articleSet = new HashSet<>();
        try {

            if(new File("/home/student/articleSet.out").isFile()) {
                FileInputStream fis = new FileInputStream("/home/student/articleSet.out");
                ObjectInputStream oin = new ObjectInputStream(fis);
                //article = (Article) oin.readObject();
                articleSet = (Set<Article>) oin.readObject();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        //System.out.println(article.toString());
        System.out.println(articleSet.toString());

    }
}
