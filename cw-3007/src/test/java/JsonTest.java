import com.company.domain.Person;
import com.company.service.JsonMapperService;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class JsonTest {

    @Test
    public void testConvertToJson() {
        JsonMapperService jsonMapperService = new JsonMapperService();
        String result = jsonMapperService.map(new Person(1,"JUnit", "06.07.2019"));
        assertEquals("{\"id\":1,\"name\":\"JUnit\",\"birthday\":\"06.07.2019\"}", result);
    }

    @Test
    public void testConvertToJsonNegative() {
        JsonMapperService jsonMapperService = new JsonMapperService();
        String result = jsonMapperService.map(new Person(2,"JUnit", "06.07.2010"));
        assertNotEquals("{\"id\":1,\"name\":\"JUnit\",\"birthday\":\"06.07.2019\"}", result);
    }

    @Test
    public void testConvertFromJson() {
        JsonMapperService jsonMapperService = new JsonMapperService();
        Person result = jsonMapperService.unmap("{\"id\":1,\"name\":\"JUnit\",\"birthday\":\"06.07.2019\"}");
        assertEquals(new Person(1,"JUnit", "06.07.2019"), result);
    }

    @Test
    public void testConvertFromJsonNegative() {
        JsonMapperService jsonMapperService = new JsonMapperService();
        Person result = jsonMapperService.unmap("{\"id\":1,\"name\":\"JUnit\",\"birthday\":\"06.07.2019\"}");
        assertNotEquals(new Person(2,"JUnit", "06.07.2010"), result);
    }

    @Test
    public void testConvertFromJson2() {
        JsonMapperService jsonMapperService = new JsonMapperService();
        Person result = (Person) jsonMapperService.unmapObject("{\"id\":1,\"name\":\"JUnit\",\"birthday\":\"06.07.2019\"}", Person.class);
        assertEquals(new Person(1,"JUnit", "06.07.2019"), result);
    }

    @Test
    public void testConvertFromJsonNegative2() {
        JsonMapperService jsonMapperService = new JsonMapperService();
        Person result = (Person) jsonMapperService.unmapObject("{\"id\":1,\"name\":\"JUnit\",\"birthday\":\"06.07.2019\"}", Person.class);
        assertNotEquals(new Person(2,"JUnit", "06.07.2010"), result);
    }
}
