package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Random rand = new Random();
        int count = rand.nextInt(5) + 1;
        System.out.println("Please enter information about " + count + " students.");
        Student[] students = new Student[count];
        for (int j = 0; j < count; j++) {
            boolean fl = true;
            while (fl) {
                System.out.print("Please enter information about " + (j + 1) + " student (four fields - ID (number) NAME LASTNAME MARK (number)): ");
                try {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                    String readValue = reader.readLine();
                    String[] str = readValue.split(" ");
                    if (str.length != 4)
                    {
                        System.out.print("Input string containts not four fields. ");
                    }
                    else {
                        students[j] = new Student(Integer.parseInt(str[0]), str[1], str[2],  Integer.parseInt(str[3]));
                        fl = false;
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                catch (NumberFormatException e) {
                    System.out.print("First and fourth fields must be number. ");
                }
            }
        }
        System.out.println("Descending sorted list of student by marks");
        Arrays.sort(students, Collections.reverseOrder());
        for (int i = 0; i < count; i++) {
            System.out.println(students[i].toString());
        }
    }
}
