package com.sc;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class MyThirdTest {

        @Test
        public void testWordsCount() {
            WordsCount wordsCount = new WordsCount();
            List<String> lines = new ArrayList<>();
            lines.add("asd qwe qwe");
            HashMap<String, Integer> wordsCnt = wordsCount.countWordsInFile("test.txt");
            HashMap<String, Integer> expectedResult = new HashMap<>();
            expectedResult.put("qwe", 3);
            expectedResult.put("asd", 1);
            assertEquals(expectedResult, wordsCnt);
        }

        @Test
        public void testWordsCount2() {
            assertEquals("HelloWorld", "HelloWorld");
        }
}
