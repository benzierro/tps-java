package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
       /* Tree t1 = new Tree();
        t1.setHeight(5);
        t1.setName("Tree");
        Tree t2 = new Tree();
        t2.setHeight(5);
        t2.setName("Tree");

        System.out.println(t1.equals(t2));
        System.out.println(t1.hashCode() + " string = " + t1.toString());*/

        Tree[] array = new Tree[5];
        array[0] = new Tree();
        array[0].setHeight(5);
        array[0].setName("Tree1");
        array[1] = new Tree();
        array[1].setHeight(12);
        array[1].setName("Tree2");
        array[2] = new Tree();
        array[2].setHeight(-3);
        array[2].setName("Tree3");
        array[3] = new Tree();
        array[3].setHeight(23);
        array[3].setName("Tree4");
        array[4] = new Tree();
        array[4].setHeight(13);
        array[4].setName("Tree5");

        int maxElement = 0;
        for (int i = 1; i < array.length; i++) {
            if (array[i].compareTo(array[maxElement]) > 0) {
                maxElement = i;
            }
        }
        System.out.println("Greatest element of array is " + array[maxElement].toString());

    }
}
