package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        Map<String , List<String>> booksMap = new HashMap<>();
        List<String> books1 = new ArrayList<>();
        books1.add("Book 1 author 1");
        books1.add("Book 2 author 1");
        books1.add("Book 3 author 1");
        List<String> books2 = new ArrayList<>();
        books2.add("Book 1 author 2");
        books2.add("Book 2 author 2");
        List<String> books3 = new ArrayList<>();
        books3.add("Book 1 author 3");
        books3.add("Book 2 author 3");
        books3.add("Book 3 author 3");
        booksMap.put("Author1", books1);
        booksMap.put("Author2", books2);
        booksMap.put("Author3", books3);

        System.out.println("Books of author 1 is " + booksMap.get("Author1").toString());
        System.out.println("Books of author 2 is " + booksMap.get("Author2").toString());
        System.out.println("Books of author 3 is " + booksMap.get("Author3").toString());


        TreeSet<Person> personTreeSet = new TreeSet<>();
        personTreeSet.add(new Person(10, "t"));
        personTreeSet.add(new Person(11, "t"));
        personTreeSet.add(new Person(12, "t"));
        personTreeSet.add(new Person(17, "t"));
        personTreeSet.add(new Person(18, "t"));
        personTreeSet.add(new Person(19, "t"));
        personTreeSet.add(new Person(13, "t"));
        personTreeSet.add(new Person(10, "t"));
        personTreeSet.add(new Person(14, "t"));
        personTreeSet.add(new Person(15, "t"));
        personTreeSet.add(new Person(16, "t"));
        System.out.println(personTreeSet.toString());

    }

}
