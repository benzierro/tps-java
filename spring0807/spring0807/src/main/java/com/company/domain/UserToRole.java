package com.company.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "USER_TO_ROLE", schema = "CATALOG")
public class UserToRole {

    @Id
    private int id;
    private int user_id;
    private int role_id;
}
