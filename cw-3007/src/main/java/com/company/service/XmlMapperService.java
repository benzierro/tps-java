package com.company.service;

import com.company.domain.Person;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class XmlMapperService implements MapperService {

    private static ObjectMapper objectMapper;

    static {
        objectMapper = new XmlMapper();
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
    }

    @Override
    public String map(Object object) {
        String result = "";
        try {
            result = objectMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;

    }

    @Override
    public Person unmap(String str) {
        Person result = null;
        try {
            result = objectMapper.readValue(str, Person.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Object unmapObject(String str, Class classOfT) {
        Object result = null;
        try {
            result = objectMapper.readValue(str, classOfT);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public ArrayList<Person> unmapArray(String str) {
        return null;
    }

    @Override
    public void saveToFile(Object object, String path) {
        try
        {
            Files.write(Paths.get(path), objectMapper.writeValueAsString(object).getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object readFromFile(Class classOfT, String path) {
        return null;
    }
}
